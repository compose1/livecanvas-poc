package de.appsonair.compose.live_canvas

import androidx.compose.foundation.Canvas
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.drawscope.DrawScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.isActive
import kotlinx.coroutines.withContext
import java.io.File
import java.nio.file.FileSystems
import java.nio.file.StandardWatchEventKinds
import javax.script.ScriptEngine
import javax.script.ScriptEngineManager

/**
 * apps on air
 *
 * @author Timo Drick
 */

interface LiveDrawer<T> {
    fun invoke(context: DrawScope, params: T)
}

data class SourceData(val lineNumber: Int, val packgeName: String, val engine: ScriptEngine)

@Composable
fun LiveCanvas(file: File, modifier: Modifier, onDraw: DrawScope.(Unit) -> Unit) {
    LiveCanvas(file, modifier, Unit, onDraw)
}
@Composable
fun <T: Any>LiveCanvas(file: File, modifier: Modifier, params: T, onDraw: DrawScope.(T) -> Unit) {
    val sourceData = remember(file) {
        val stackTraces = Thread.currentThread().stackTrace
        // Search myself
        val selfIndex = stackTraces.indexOfFirst { it.methodName == "LiveCanvas" }
        // take the calling trace (Which is what we want)
        val result = stackTraces[selfIndex + 1]
        val clazz = Class.forName(result.className)
        val engine = ScriptEngineManager(clazz.classLoader).getEngineByExtension("kts")
        SourceData(result.lineNumber, clazz.packageName, engine)
    }
    val fileContent = FileAsState(file)
    val liveCode = remember(fileContent) {
        //println(sourceData.toString())
        val programCode = extractProgramCode(file, sourceData)
        val code = generateLiveDrawerScript(programCode, sourceData, checkNotNull(params::class.qualifiedName))
        println(code)
        try {
            sourceData.engine.eval(code) as LiveDrawer<T>
        } catch (err: Throwable) {
            err.printStackTrace()
            null
        }
    }
    //println(fileContent)
    Canvas(modifier) {
        liveCode?.invoke(this, params)
    }
}

data class ProgramCode(val rawCode: String, val imports: String)

fun extractProgramCode(file: File, sourceData: SourceData): ProgramCode {
    val fileLines = file.readText().lines()
    // extract imports
    val imports = fileLines.filter { it.trim().startsWith("import") }.joinToString("\n")
    //Decided to scan the whole file for the pre/postfix so when imports are added the line number does not matter.
    val startList = fileLines//fileLines.subList(sourceData.lineNumber, fileLines.size - 1)
    val startIndex = startList.indexOfFirst { it.trimStart().startsWith("//LIVE_CODE_START") }
    val endIndex = startList.indexOfFirst { it.trimStart().startsWith("//LIVE_CODE_END") }
    val rawCode = startList.subList(startIndex + 1, endIndex).joinToString("\n")
    return ProgramCode(rawCode, imports)
}

private val liveDrawerImport = LiveDrawer::class.qualifiedName
private val drawScopeImport = DrawScope::class.qualifiedName
fun generateLiveDrawerScript(code: ProgramCode, sourceData: SourceData, paramClazz: String) = """
//package ${sourceData.packgeName}

${code.imports}
import $liveDrawerImport
import $drawScopeImport

object : LiveDrawer<$paramClazz> {
    override fun invoke(context: DrawScope, params: $paramClazz) = with(context) {
        ${code.rawCode}
    }
}
    """.trimIndent()

@Composable
fun FileAsState(file: File): String {
    var fileContent by remember(file) { mutableStateOf(file.readText()) }
    LaunchedEffect(file) {
        withContext(Dispatchers.IO) {
            val watchService = FileSystems.getDefault().newWatchService()
            file.parentFile.toPath().register(watchService, StandardWatchEventKinds.ENTRY_MODIFY)
            while (isActive) {
                val watchKey = watchService.take()
                for (event in watchKey.pollEvents()) {
                    println("event: ${event.kind()} context: ${event.context()} ${event.count()}")
                }
                fileContent = file.readText()
                if (!watchKey.reset()) {
                    watchKey.cancel()
                    watchService.close()
                    break
                }
            }
        }
    }
    return fileContent
}
