package test

import androidx.compose.animation.core.withInfiniteAnimationFrameMillis
import androidx.compose.desktop.Window
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import de.appsonair.compose.live_canvas.LiveCanvas
import kotlinx.coroutines.isActive
import java.io.File
import kotlin.math.sin

data class LiveParams(val seconds: Double)

fun main() {
    Window {
        MaterialTheme {
            var text by remember { mutableStateOf("Hello, World!") }
            Button(onClick = {
                text = "Hello, Desktop!"
            }) {
                Text(text)
            }
            var seconds by remember { mutableStateOf(0.0) }
            LaunchedEffect(key1 = Unit) {
                val startMillis = withFrameMillis { it }
                while (isActive) {
                    withInfiniteAnimationFrameMillis {
                        val duration = it - startMillis
                        seconds = duration.toDouble() / 1000.0
                    }
                }
            }
            LiveCanvas(
                file = File("src/main/kotlin/test/main.kt"),
                modifier = Modifier.fillMaxSize(),
                params = LiveParams(seconds)) { params ->
                //LIVE_CODE_START
                val radius = sin(params.seconds).toFloat() + 1f
                drawCircle(Color.Green)
                drawCircle(Color.LightGray, radius = radius * 100f, center = Offset(160f, 300f))
                drawCircle(Color.Yellow, radius = 150f, center = Offset(radius * 400f, 300f))
                //drawLine(Color.Black, Offset(0f, 0f), Offset(size.width, size.height))
                //LIVE_CODE_END
            }
        }
    }
}
